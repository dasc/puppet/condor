# condor

#### Table of Contents

1. [Description](#description)
1. [Setup - The basics of getting started with condor](#setup)
    * [What condor affects](#what-condor-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with condor](#beginning-with-condor)
1. [Usage - Configuration options and additional functionality](#usage)
1. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
1. [Limitations - OS compatibility, etc.](#limitations)

## Description

This module will configure the condor job scheduling service.

The condor services will be installed, and hooks to install condor config files are provided.

## Setup

### What condor affects

The condor package is installed.  New files in /etc/condor are created.

### Setup Requirements

You will need to provide condor config files to be placed in /etc/condor and /etc/condor/config.d.  This module does not use templates to generate these config files due to the very large number of possible condor settings available.

Inside the file store where you put the condor config files you will need a etc/condor directory.  This module will look for your named config files in ${localfilestore}/etc/condor/${shortname_you_provide}

### Beginning with condor

The very basic steps needed for a user to get the module up and running. This
can include setup steps, if necessary, or it can be an example of the most
basic use of the module.

## Usage

To install the condor service, assuming all necessary condor config settings are in modules/profile/files/etc/condor/condor_config and modules/profile/files/etc/condor/condor_config.local:

```
    class {'::condor':
        package_ensure      => '8.8.7-1.el7',
        localfilestore      => "puppet://puppet/modules/profile",
	condor_config       => 'condor_config',
	condor_config_local => 'condor_config.local',
    }
```

To install additional condor config files in /etc/condor/config.d, specify where they should come from.  This example will install modules/profile/files/etc/condor/config.d/00-logging into /etc/condor/confid.d/00-logging on the puppet client:

```
    $localfilestore = 'file://nfs-host/condor/config.d'
    condor::configfile { '00-logging': filestore => "${localfilestore}"}
```

If the config.d files and global condor_config files come from the same source filestore, you can omit it from the condor::configfile resources:

```
    class {'::condor':
        package_ensure      => '8.8.7-1.el7',
        localfilestore      => "puppet://puppet/modules/profile",
	condor_config       => 'condor_config',
	condor_config_local => 'condor_config.local',
    }
    condor::configfile { '00-logging': }
```

To ensure that a config file is not present in /etc/condor/config.d, use the ensure attribute:

```
    condor::configfile { '20-schedd': ensure => absent, }
```

You can use hiera to override the local filename in the local filestore.  This example will install modules/profile/files/etc/condor/config.d/20-schedd.pcdev2 into /etc/condor/confid.d/20-schedd on the puppet client:

```
condor::configfile::20-schedd: '20-schedd.pcdev2'
```

```
    condor::configfile { '20-schedd': }
```


## Reference

Users need a complete list of your module's classes, types, defined types providers, facts, and functions, along with the parameters for each. You can provide this list either via Puppet Strings code comments or as a complete list in this Reference section.

* If you are using Puppet Strings code comments, this Reference section should include Strings information so that your users know how to access your documentation.

* If you are not using Puppet Strings, include a list of all of your classes, defined types, and so on, along with their parameters. Each element in this listing should include:

  * The data type, if applicable.
  * A description of what the element does.
  * Valid values, if the data type doesn't make it obvious.
  * Default value, if any.

## Limitations

This module has only been tested on RHEL7-compatible systems, but may work on other distributions.
