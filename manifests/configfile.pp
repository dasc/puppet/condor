#@param filename Optional[String]
#
#The short filename to be created.  If not specified, then it defaults to the resource title.
#
#
#@param ensure Optional[Enum]
#
#Indicates if the config file should exist or not.  Must be one of 'file' or 'absent'.  This is one way to ensure that certain config files don't exist.  Defaults to 'true'
#
#
#@param filestore Optional[String]
#
#The URI prefix that will be prepended to the filename to generate the source for the configuration file.  If not specified, then the filestore from the condor class will be used.
#
#
#@param content Optional[String]
#
#The content of the configuration file.  This is used when the configuration file contents come from a template.
#
#
#@param source Optional[String]
#
#The source URI for the configuration file.  This setting is ignored if 'content' is used.  If neither 'content' or 'source' is specified, then a source URI is constructed using the filestore and the filename.
#
#
#@param reconfig Optional[Enum]
#
#The name of a condor subsystem to reconfigure when this configuration file is changed.  Must be one of 'startd', 'schedd', or 'master'.  Defaults to 'master'.


define condor::configfile ($filename=$title, $ensure = 'file', $filestore = "${::condor::filestore}", $content = undef, $source = undef, $reconfig = 'master') {

    #notify {"condorfilestore ${title}": message => "${::condor::localfilestore}", }
    $configfile = lookup("condor::configfile::${title}", { default_value => "${filename}"})

    if $content {
        #notify {"condor::configfile ${filename} string content": message => "content is ${content}", }
        file { "/etc/condor/config.d/${filename}":
            ensure  => "${ensure}",
            owner   => 'root',
            group   => 'root',
            mode    => '0644',
            content => "${content}",
            notify  => Exec["condor_reconfig $reconfig"],
            require => Package['condor'],
        }
    } elsif $source {
        #notify {"condor::configfile ${filename} source": message => "source is ${content}", }
        file { "/etc/condor/config.d/${filename}":
            ensure  => "${ensure}",
            owner   => 'root',
            group   => 'root',
            mode    => '0644',
            source  => "${source}",
            notify  => Exec["condor_reconfig $reconfig"],
            require => Package['condor'],
        }
    } else {
        #notify {"condor::configfile ${filename} default": message => "Copying file from ${filestore}/${configfile}", }
        file { "/etc/condor/config.d/${filename}":
            ensure  => "${ensure}",
            owner   => 'root',
            group   => 'root',
            mode    => '0644',
            source  => "${filestore}/${configfile}",
            notify  => Exec["condor_reconfig $reconfig"],
            require => Package['condor'],
        }
    }
}
