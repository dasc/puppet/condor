# The condor class
#
# @summary Install the condor package.  Required directories are created.  Helper resources are created to aid in restarting specific subsystems.  Any condor::configfile attributes found in hiera will be applied.
#
# @example Basic usage
#   class {'::condor':
#       package_ensure      => '8.8.7-1.el7',
#       filestore      => "puppet://puppet/modules/profile",
#   }
#   condor::configfile { '00-logging': }
#   condor::configfile { '20-schedd': }
#
# @param package_ensure [String]
#   What state the condor package should be in, or a specific version number
#
# @param install_options Optional[Hash]
#   Any additional install_options that will be passed to the package{} declaration.  This is where you can specify the repository that the package should come from.
#
# @param service_ensure Optional[Enum['stopped', 'running']]
#   One of 'stopped' or 'running'.  If not specified, then no action is taken, and the service must be managed through some other means (eg manually, or monit)
#
# @param filestore Optional[String]
#   A puppet file store URL for locating the config file sources
#
# @param reconfig_daemon Optional[Enum['master', 'schedd', 'startd']]
#   The name of the daemon to reconfig when config file changes are made
#
# @param extra_config Optional[Array[String]]
#   A set of extra configuration settings to put into condor_config.local.  This option should usually come from hiera and not specified when the class is declared.

class condor (
    String $package_ensure,
    Optional[Enum['stopped', 'running']] $service_ensure = undef,
    Optional[String] $filestore = undef,
    Optional[Hash]   $install_options = undef,
    Optional[Array[String]] $extra_config = undef,
    Optional[Enum['master', 'schedd', 'startd']] $reconfig_daemon = 'master',
) {
    package {'condor': ensure => $package_ensure,
	install_options => $install_options,
	notify          => Exec['condor_reconfig master'],
    } ->
    package {'python3-condor':
        ensure          => $package_ensure,
	install_options => $install_options,
    }
    if $facts['os']['family'] == 'RedHat' and $facts['os']['release']['major'] < '8' {
        package {'python2-condor':
            ensure          => $package_ensure,
            install_options => $install_options,
            require         => Package['condor'],
        }
    }

    if $service_ensure {
        service {'condor':
            ensure => $service_ensure,
        }
    } else {
        service {'condor': }
    }

    file { '/etc/condor/config.d':
        ensure  => 'directory',
        owner   => 'root',
        group   => 'root',
	mode    => '0755',
	require => Package['condor'],
    }
    file { '/etc/condor/modules':
        ensure  => 'directory',
        owner   => 'root',
        group   => 'root',
        mode    => '0755',
	require => Package['condor'],
    }

    $configfiles = lookup('condor::configfile', {default_value => {}})
    $configfiles.each |$configname, $configdata| {
        if !$configdata {
            condor::configfile{ $configname: }
        } else {
            if $configdata['template'] {
                $newconfigdata = $configdata.delete('template') + { content => template($configdata['template'])}
            } else {
                $newconfigdata = $configdata
            }
            condor::configfile{ $configname:
                *       => $newconfigdata,
                require => Package['condor'],
            }
        }
    }

    ['master', 'schedd', 'startd'].each |$daemon| {
        exec { "condor_reconfig ${daemon}":
            command     => "/usr/sbin/condor_reconfig -daemon ${daemon}",
            refreshonly => true,
        }
    }

    file {'/etc/condor/condor_config.local':
        ensure => 'file',
        owner  => 'root',
        group  => 'root',
        mode   => '0644',
        content => epp('condor/etc/condor/condor_config.local.epp', {
            extra_config => $extra_config,
        }),
        notify => Exec['condor_reconfig master'],
        require => Package['condor'],
    }
}
